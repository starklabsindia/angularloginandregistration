import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  public fullName:string;
  public mobile: string;
  public email: string;
  public password: string;

  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private as: AuthService
  ) {

    this.registerFormSuper();
   }

  ngOnInit() {
  }
  registerFormSuper() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  register(fullName: any, email:any,  mobile: any, password: any) {

    const rData = {
      fullName,
      email,
      mobile,
      password
    }

    this.as.registerService(rData).subscribe(_res => {
      console.log('Done');
      this.router.navigate(['/login']);
    });
    


  }

}

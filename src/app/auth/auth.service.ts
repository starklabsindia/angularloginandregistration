import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { Tokens } from './tokens';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  uri = 'http://localhost:4000/api/user';

  constructor(
    private http: HttpClient,

    private router: Router,
  ) { }

  loginService(data: { mobile: any; password: any; }){      
    
    console.log(data);
       
    return this.http.post<any>('http://localhost:4000/api/user/login', data, httpOptions)   

  }

  

  registerService(rData: { fullName: any; email:any; mobile: any; password: any; }){
    let obj = {
      name : rData.fullName,
      email: rData.email,
      mobile: rData.mobile,
      password: rData.password
    };
    return this.http.post<any>('http://localhost:4000/api/user/register', obj, httpOptions)
    
  }

  logoutUser() {
    localStorage.removeItem('token')
    this.router.navigate(['/login'])
  }


  getToken() {
    return localStorage.getItem('token')
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }
}

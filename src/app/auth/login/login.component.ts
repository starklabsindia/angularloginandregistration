import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   loginForm: FormGroup;
   public mobile:string;
   public password:string;
  
  constructor(
    private formBuilder: FormBuilder,
    private as: AuthService, 
    private router: Router
    
  ) { 
    this.loginFormSuper();
  }

  loginFormSuper(){
    this.loginForm = this.formBuilder.group({
      mobile: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    
  }

  login(mobile: any, password: any){

   const data = {
     mobile,
     password
   }
   
    this.as.loginService(data).subscribe(_res => {
      localStorage.setItem('token', _res.token)
      this.router.navigate(['/dashboard'])
    });
   

  }


  


}
